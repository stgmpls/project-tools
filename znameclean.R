nameclean <- function( tablein ) {
  require(stringr)
  tableout <- tablein
  colnames( tableout ) %<>%
    str_replace_all( "[^[:alnum:]]", "" ) %>%
    tolower
  return( tableout )
}